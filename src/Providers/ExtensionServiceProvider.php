<?php

namespace Sinta\Laravel\Addons\Providers;


use Illuminate\Support\Facades\Blade;
use Sinta\Laravel\Addons\AliasResolver;
use Sinta\Laravel\Addons\BladeExtension;
use Sinta\Laravel\Addons\ClassLoader;
use Sinta\Laravel\Addons\Registrar;
use Sinta\Laravel\Addons\Repository;
use Sinta\Laravel\Addons\Events;
use Sinta\Laravel\Addons\Environment as AddonEnvironment;
use Sinta\Laravel\Addons\Generator as AddonGenerator;
use Illuminate\Support\ServiceProvider;

class ExtensionServiceProvider extends ServiceProvider
{

    /**
     * @var \Sinta\Laravel\Addons\Environment
     */
    protected $environment;

    /**
     * @var array 数组
     */
    protected $addons;


    public function register()
    {
        $app = $this->app;

        //注册spec
        $app['path.specs'] = $app->basePath()."/resources/specs";

        //注册spec repository
        $app->singleton('specs',function($app){
            $loader = new Repository\FileLoader($app['files'],$app['path.specs']);
            return new Repository\NamespacedRepository($loader);
        });

        //注册addon环境
        $app->instance('addon', $this->environment = new AddonEnvironment($app));
        $app->alias('addon', AddonEnvironment::class);

        //注册addon generator
        $app->singleton('addon.generator', function ($app) {
            return new AddonGenerator();
        });
        $app->alias('addon.generator', AddonGenerator::class);

        //注册数据库migrator
        $app->singleton('database.migrator',function($app){
            return new Migrator($app['db'], $app['config']);
        });
        $app->alias('database.migrator', Migrator::class);


        $app['events']->fire(new Events\AddonWorldCreated($this->environment));
        $this->registerClassResolvers();
        (new Registrar())->register($app, $this->environment->addons());
        $app['events']->fire(new Events\AddonRegistered($this->environment));
    }


    /**
     *
     */
    protected function registerClassResolvers()
    {
        $addons = $this->environment->addons();
        ClassLoader::register($this->environment, $addons);
        AliasResolver::register($this->app['path'], $addons, $this->app['config']->get('app.aliases', []));
    }

    public function boot()
    {
        $app = $this->app;

        $this->registerBladeExtensions();

        (new Registrar)->boot($app, $this->environment->addons());
        $app['events']->fire(new Events\AddonBooted($this->environment));
    }

    protected function registerBladeExtensions()
    {
        Blade::extend(BladeExtension::comment());

        Blade::extend(BladeExtension::script());
    }
}