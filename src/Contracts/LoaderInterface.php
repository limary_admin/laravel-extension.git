<?php

namespace Sinta\Laravel\Addons\Contracts;

interface LoaderInterface
{
    public function load($group, $namespace = null);

    public function exists($group, $namespace = null);

    public function addNamespace($namespace, $hint);

    public function getNamespaces();
}