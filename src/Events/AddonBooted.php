<?php

namespace Sinta\Laravel\Addons\Events;

use Sinta\Laravel\Addons\Environment;

class AddonBooted
{
    public $world;

    public function __construct(Environment $world)
    {
        $this->world = $world;
    }
}